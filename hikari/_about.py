# -*- coding: utf-8 -*-
# cython: language_level=3
# Copyright (c) 2020 Nekokatt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
"""Package metadata."""

# DO NOT ADD TYPE HINTS TO THESE FIELDS. THESE ARE AUTOMATICALLY UPDATED
# FROM THE CI SCRIPT AND DOING THIS MAY LEAD TO THE DEPLOY PROCESS FAILING.

__author__ = "Nekokatt"
__ci__ = "https://gitlab.com/nekokatt/hikari/pipelines"
__copyright__ = "© 2020 Nekokatt"
__discord_invite__ = "https://discord.gg/Jx4cNGG"
__docs__ = "https://nekokatt.gitlab.io/hikari"
__email__ = "3903853-nekokatt@users.noreply.gitlab.com"
__issue_tracker__ = "https://gitlab.com/nekokatt/hikari/issues"
__is_official_distributed_release__ = False
__license__ = "MIT"
__url__ = "https://gitlab.com/nekokatt/hikari"
__version__ = "2.0.0.dev0"
__git_branch__ = "development"
__git_sha1__ = "HEAD"
__git_when__ = "2020/07/06"
